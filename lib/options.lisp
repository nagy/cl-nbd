(uiop:define-package :nbd/lib/options
    (:use :cl)
  (:import-from :flexi-streams
                #:get-output-stream-sequence
                #:make-in-memory-output-stream
                #:octets-to-string
                #:string-to-octets
                #:with-input-from-sequence)
  (:import-from :lisp-binary
                #:counted-array
                #:counted-string
                #:defbinary
                #:magic
                #:read-binary
                #:read-integer
                #:write-integer
                #:write-binary)
  (:export #:info-name
           #:info-requests
           #:option-type
           #:option-value
           #:read-option
           #:reply
           #:reply-error))

(in-package :nbd/lib/options)

(defvar *options-types*
  '((1 . :export-name)
    (2 . :abort)
    (3 . :list)
    (4 . :peek-export) ;; unused
    (5 . :starttls)
    (6 . :info)
    (7 . :go)
    (8 . :structured-reply)
    (9 . :list-meta-context)
    (10 . :set-meta-context)))

(defvar *reply-types*
  '((:ack . 1)
    (:server . 2)
    (:info . 3)
    (:meta-context . 4)))

(defvar *error-reply-types*
  (mapcar
   (lambda (a)
     ;; set 31st bit
     (list* (car a) (dpb 1 (byte 32 30) (cdr a))))
   '((:unsup . 1)
     (:policy . 2)
     (:invalid . 3)
     (:platform . 4)
     (:tls-reqd . 5)
     (:unknown . 6)
     (:shutdown . 7)
     (:block-size-reqd . 8)
     (:too-big . 9))))

(defbinary %option (:byte-order :big-endian)
  (magic #x49484156454F5054 :type (magic :actual-type 64
                                         :value #x49484156454F5054))
  (type 0 :type 32)
  (data (make-array 0 :element-type '(unsigned-byte 8))
        :type (counted-array 4 (unsigned-byte 8))))

(defclass option ()
  ((type :initarg :type :reader option-type)
   (value :initarg :value :reader option-value)))

(defun read-option (stream)
  (let* ((raw-option (read-binary '%option stream))
         (keyword (integer-to-keyword raw-option))
         (value (casted-value keyword (%option-data raw-option))))
    (make-instance 'option :type keyword :value value)))

(defun integer-to-keyword (%option)
  (cdr (assoc (%option-type %option) *options-types*)))

(defun keyword-to-integer (option)
  (car (rassoc (option-type option) *options-types*)))

(defbinary %opt-info-and-go (:byte-order :big-endian)
  (name "" :type (counted-string 4 :external-format :utf8))
  (requests-count 0 :type 16)
  (requests (make-array 0 :element-type '(unsigned-byte 16))
            :type (simple-array (unsigned-byte 16) (requests-count))))

(defclass info ()
  ((name :initarg :name :reader info-name)
   (requests :initarg :requests :reader info-requests)))

(defvar *info-types*
  '((0 . :export)
    (1 . :name)
    (2 . :description)
    (3 . :block-size)))

(defun casted-value (type data)
  (case type
    (:export-name (octets-to-string data :external-format :utf8))
    ((or :info :go)
     (with-input-from-sequence (stream data)
       (let ((opt (read-binary '%opt-info-and-go stream)))
         (make-instance
          'info
          :name (%opt-info-and-go-name opt)
          :requests (mapcar
                     (lambda (info-type-number)
                       (cdr (assoc info-type-number *info-types*)))
                     (coerce (%opt-info-and-go-requests opt) 'list))))))
    (:list-meta-context nil) ; todo
    (:set-meta-context nil) ; todo
    ))

(defbinary reply (:byte-order :big-endian)
  (magic #x3e889045565a9 :type (magic :actual-type 64
                                      :value #x3e889045565a9))
  (option-type 0 :type 32)
  (reply-type 0 :type 32)
  (data (make-array 0 :element-type '(unsigned-byte 8))
        :type (counted-array 4 (unsigned-byte 8))))

(defgeneric reply (option stream type &optional data)
  (:documentation "Implements replies to options."))

(defmacro defreply (type (type-value option-value stream &optional data) &body body)
  (let ((type-identifier (gensym))
        (option-identifier (gensym))
        (data-identifier (or data (gensym))))
    `(defmethod reply ((,option-identifier option) ,stream (,type-identifier (eql ,type))
                       &optional ,data-identifier)
       (declare (ignorable ,data-identifier))
       (let ((,type-value (cdr (assoc ,type-identifier *reply-types*)))
             (,option-value (keyword-to-integer ,option-identifier)))
         ,@body))))

(defreply :ack
    (type-value option-value stream)
  (write-binary (make-reply :option-type option-value
                            :reply-type type-value)
                stream))

(defbinary server-reply (:byte-order :big-endian)
  (data (make-array 0 :element-type '(unsigned-byte 8))
        :type (counted-array 4 (unsigned-byte 8))))

(defreply :server
    (type-value option-value stream export-name)
  (let ((in-memory-stream (make-in-memory-output-stream)))
    (write-binary (make-server-reply
                   :data (string-to-octets export-name :external-format :utf-8))
                  in-memory-stream)
    (write-binary (make-reply :option-type option-value
                              :reply-type type-value
                              :data (copy-seq (get-output-stream-sequence in-memory-stream)))
                  stream)))

(defreply :info
    (type-value option-value stream tuple)
  (let ((rep-type (first tuple))
        (in-memory-stream (make-in-memory-output-stream)))
    ;; info type first
    (write-integer
     (car (rassoc rep-type *info-types*)) 2 in-memory-stream :byte-order :big-endian)
    ;; per-info-type info
    (case rep-type
      (:export
       (let ((export-size (second tuple))
             (transmission-flags (third tuple)))
         (write-integer export-size 8 in-memory-stream :byte-order :big-endian)
         (write-integer transmission-flags 2 in-memory-stream :byte-order :big-endian)))
      (:name
       (let ((export-name (second tuple)))
         (write-sequence (string-to-octets export-name :external-format :utf8)
                         in-memory-stream)))
      (:description
       (let ((export-description (second tuple)))
         (write-sequence (string-to-octets export-description :external-format :utf8)
                         in-memory-stream)))
      (:block-size
       (let ((min-block-size (second tuple))
             (preferred-block-size (third tuple))
             (max-block-size (fourth tuple)))
         (write-integer min-block-size 4 in-memory-stream :byte-order :big-endian)
         (write-integer preferred-block-size 4 in-memory-stream :byte-order :big-endian)
         (write-integer max-block-size 4 in-memory-stream :byte-order :big-endian))))
    ;; the actual reply
    (write-binary
     (make-reply :option-type option-value
                 :reply-type type-value
                 :data (copy-seq (get-output-stream-sequence in-memory-stream)))
     stream)))

(defun reply-error (option stream error)
  (write-binary (make-reply :option-type (keyword-to-integer option)
                            :reply-type (cdr (assoc error *error-reply-types*)))
                stream))
