(uiop:define-package :nbd/lib/transmission
    (:use :cl :nbd/lib/shared :nbd/lib/transmission-flags)
  (:import-from :lisp-binary
                #:defbinary
                #:magic
                #:read-binary
                #:write-binary)
  (:export #:transmission))

(in-package :nbd/lib/transmission)

(defvar *request-types*
  '((0 . :read)
    (1 . :write)
    (2 . :disc)
    (3 . :flush)
    (4 . :trim)
    ;; Everything below is unsupported for now.
    (5 . :cache)
    (6 . :write-zeroes)
    (7 . :block-status)
    (8 . :resize)))

(defvar *errors*
  '((:ok . 0)
    (:eperm . 1)
    (:eio . 5)
    (:enomem . 12)
    (:einval . 22)
    (:enospc . 28)
    (:eoverflow . 75)
    (:enotsup . 95)
    (:eshutdown . 108)))

(defvar *command-flags*
  '((#b1 . :flag-fua)
    (#b10 . :flag-no-hole)
    (#b100 . :flag-df)
    (#b1000 . :flag-req-one)
    (#b10000 . :flag-fast-zero)))

(define-condition validation-error (error)
  ((error-code :initarg :error-code :initform :einval :reader validation-error-code)
   (message :initarg :message :initform "" :reader validation-error-message)))

(defun flag-value (keyword)
  (car (rassoc keyword *command-flags*)))

(defbinary %request (:byte-order :big-endian)
  (magic #x25609513 :type (magic :actual-type 32
                                 :value #x25609513))
  ;; todo: we don't support any command flags at the moment.
  (command-flags 0 :type 16)
  (type 0 :type 16)
  (handle 0 :type 64)
  (offset 0 :type 64)
  (length 0 :type 32)
  ;; We could make a (counted-array 4) there but the length field for
  ;; CMD_READ would allocate an empty array and not use it, which
  ;; would be wasteful use of memory. Instead, we have to read the
  ;; data when we know if it's a read or a write.
  )

(defun transmission (server client)
  (let* ((stream (client-stream client)))
    (macrolet ((with-flush (&body body)
                 `(unwind-protect
                       (progn ,@body)
                    (finish-output stream))))
      (loop
        (block request
          (let* ((on-request (server-on-request server))
                 (request (read-binary '%request stream))
                 (request-type (cdr (assoc (%request-type request) *request-types*)))
                 (request-handle (%request-handle request))
                 (request-offset (%request-offset request))
                 (request-length (%request-length request))
                 (request-command-flags (%request-command-flags request))
                 (request-flag-fua (logtest (flag-value :flag-fua) request-command-flags))
                 (request-flags `(,@(when request-flag-fua '(:flag-fua)))))
            (handler-case
                (validate-request server request)
              (validation-error (c)
                (return-from request
                  (with-flush
                      (reply stream (validation-error-code c) request-handle)))))
            (when (member request-type '(:read :write :flush :trim))
              (return-from request
                ;; let the server decide when to flush.
                (funcall on-request
                         request-type request-flags request-handle request-offset
                         request-length stream
                         (lambda (&optional (error-code :ok))
                           (reply stream error-code request-handle)))))
            (when (eql request-type :disc)
              (return-from transmission
                (with-flush
                  (funcall on-request request-type nil 0 0 0 stream
                           (lambda (&optional (error-code :ok))
                             (declare (ignore error-code)))))))
            ;; Everything else is unsupported or broken.
            (with-flush (reply stream :einval request-handle))))))))

(defun validate-request (server request)
  (let* ((transmission-flags (server-supported-flags server))
         (read-only (logtest *read-only* transmission-flags))
         (supports-flush (logtest *send-flush* transmission-flags))
         (supports-trim (logtest *send-trim* transmission-flags))
         (supports-fua (logtest *send-fua* transmission-flags))
         (request-type (cdr (assoc (%request-type request) *request-types*)))
         (request-offset (%request-offset request))
         (request-length (%request-length request))
         (request-command-flags (%request-command-flags request))
         (request-flag-fua (logtest (flag-value :flag-fua) request-command-flags))
         (request-flag-no-hole (logtest (flag-value :flag-no-hole)
                                        request-command-flags))
         (request-flag-df (logtest (flag-value :flag-df) request-command-flags))
         (request-flag-req-one (logtest (flag-value :flag-req-one)
                                        request-command-flags))
         (request-fast-zero (logtest (flag-value :flag-fast-zero)
                                     request-command-flags)))
    (when (or request-flag-no-hole request-flag-df
              request-flag-req-one request-fast-zero)
      (error 'validation-error :message "Unsupported flags"))
    ;; flags validations
    (when (and request-flag-fua (not supports-fua))
      (error 'validation-error :message "FUA requested without server support"))
    (case request-type
      (:read
       (when (> (+ request-offset request-length) (server-export-size server))
         (error 'validation-error :message "request outside of export boundaries")))
      (:write
       (when read-only
         (error 'validation-error
                :error-code :eperm
                :message "Cannot write on a read-only export"))
       (when (> (+ request-offset request-length) (server-export-size server))
         (error 'validation-error
                :error-code :enospc
                :message "Write outside of export boundaries")))
      (:flush
       (unless supports-flush
         (error 'validation-error :message "Flush requested without server support")))
      (:trim
       (unless supports-trim
         (error 'validation-error :message "Trim requested without server support"))
       (when read-only
         (error 'validation-error
                :error-code :eperm
                :message "Cannot trim on a read-only export"))))))

(defbinary %reply (:byte-order :big-endian)
  (magic #x67446698 :type (magic :actual-type 32
                                 :value #x67446698))
  (error-code 0 :type 32)
  (handle 0 :type 64))

(defun reply (stream error-code handle)
  (write-binary (make-%reply :error-code (cdr (assoc error-code *errors*))
                             :handle handle)
                stream))
