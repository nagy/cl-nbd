(uiop:define-package :nbd/lib/shared
    (:use :cl)
  (:export #:client
           #:client-fixed-newstyle
           #:client-no-zeroes
           #:client-socket
           #:client-stream
           #:server
           #:server-accept-thread
           #:server-export-description
           #:server-export-name
           #:server-export-size
           #:server-max-block-size
           #:server-min-block-size
           #:server-on-request
           #:server-preferred-block-size
           #:server-socket
           #:server-stream
           #:server-supported-flags))

(in-package :nbd/lib/shared)

;;;; For symbols that are shared across packages without warranting
;;;; their own package.

(defclass server ()
  ((socket :initarg :socket :reader server-socket)
   (stream :initarg :stream :reader server-stream)
   (export-name :initarg :export-name :reader server-export-name)
   (export-description :initarg :export-description :reader server-export-description)
   (export-size :initarg :export-size :reader server-export-size)
   (min-block-size :initarg :min-block-size :reader server-min-block-size)
   (preferred-block-size :initarg :preferred-block-size :reader server-preferred-block-size)
   (max-block-size :initarg :max-block-size :reader server-max-block-size)
   (supported-flags :initarg :supported-flags :reader server-supported-flags)
   (on-request :initarg :on-request :reader server-on-request)
   (accept-thread :accessor server-accept-thread)))

(defclass client ()
  ((socket :initarg :socket :reader client-socket)
   (stream :initarg :stream :reader client-stream)
   (fixed-newstyle :initform t :accessor client-fixed-newstyle)
   (no-zeroes :initform t :accessor client-no-zeroes)))
