(uiop:define-package :nbd/lib/handshake
    (:use :cl :nbd/lib/shared :nbd/lib/options)
  (:import-from :lisp-binary
                #:defbinary
                #:magic
                #:read-binary
                #:write-binary
                #:write-integer)
  (:import-from :nbd/lib/transmission-flags)
  (:export #:handshake))

(in-package :nbd/lib/handshake)

(defvar *nbd-flag-fixed-newstyle* 1)

(defvar *nbd-flag-no-zeroes* 2)

(defbinary server-handshake (:byte-order :big-endian)
  (magic #x4e42444d41474943 :type (magic :actual-type 64
                                         :value #x4e42444d41474943))
  (i-have-opt #x49484156454f5054 :type (magic :actual-type 64
                                              :value #x49484156454f5054)))

(defbinary client-handshake (:byte-order :big-endian)
  (flags 0 :type 32))

(defun handshake (server client)
  (let ((stream (client-stream client)))
    (write-binary (make-server-handshake) stream)
    (write-integer (logior *nbd-flag-fixed-newstyle* *nbd-flag-no-zeroes*)
                   2
                   stream
                   :byte-order :big-endian)
    (finish-output stream)

    (let* ((client-handshake (read-binary 'client-handshake stream))
           (client-flags (client-handshake-flags client-handshake)))
      (when (> client-flags 3)
        (error "Invalid client flags"))
      (when (= (logand client-flags *nbd-flag-no-zeroes*) 0)
        (setf (client-no-zeroes client) nil))
      (when (= (logand client-flags *nbd-flag-fixed-newstyle*) 0)
        (setf (client-fixed-newstyle client) nil)))

    (if (client-fixed-newstyle client)
        (handshake-fixed-newstyle server client stream)
        (handshake-newstyle server client stream))))

(defun handshake-fixed-newstyle (server client stream)
  (macrolet ((with-flush (&body body)
               `(unwind-protect
                     (progn ,@body)
                  (finish-output stream))))
    (loop
      (let* ((option (read-option stream))
             (option-type (option-type option)))
        (with-flush
          (case option-type
            (:export-name (return-from handshake-fixed-newstyle
                            (handshake-export-name server client stream)))
            (:abort (reply option stream :ack)
             (error "Abort requested by client."))
            (:list (if (> (length (option-value option)) 0)
                       (reply-error option stream :invalid)
                       (progn
                         ;; We only support a single export.
                         (reply option stream :server (server-export-name server))
                         (reply option stream :ack))))
            (:starttls (if (> (length (option-value option)) 0)
                           (reply-error option stream :invalid)
                           ;; todo: support TLS. One day. The primary
                           ;; use case for this library at the moment
                           ;; is an NBD server over a local unix
                           ;; socket, so TLS support doesn't make a
                           ;; ton of sense, but the library should
                           ;; eventually be extended for more use
                           ;; cases.
                           (reply-error option stream :unsup)))
            ((or :info :go)
             (block info
               (let* ((info (option-value option))
                      (export-name (info-name info))
                      (requests (info-requests info))
                      (has-requested-block-size nil)
                      (can-ack nil))
                 (handler-case
                     (validate-export-name server export-name)
                   (error ()
                     (return-from info (reply-error option stream :unknown))))
                 (dolist (request requests)
                   (case request
                     (:export
                      (setf can-ack t)
                      (reply-info-export option stream server))
                     (:name (reply option stream :info `(:name ,export-name)))
                     (:description
                      (reply option stream :info `(:description
                                                   ,(server-export-description server))))
                     (:block-size
                      (setf has-requested-block-size t)
                      (reply-info-block-size option stream server))))
                 (when (and (not has-requested-block-size)
                            ;; defaults are 1/4096/export size,
                            ;; anything different should be
                            ;; advertised as such.
                            (not (= (server-min-block-size server) 1))
                            (not (= (server-preferred-block-size server) 4096))
                            (not (= (server-max-block-size server)
                                    (server-export-size server))))
                   (return-from info
                     (progn
                       (reply-error option stream :block-size-reqd))))
                 (unless can-ack
                   (if (eql option-type :go)
                       (reply-info-export option stream server)
                       (error
                        "Client didn't send any NBD_INFO_EXPORT request in NBD_OPT_INFO.")))
                 (reply option stream :ack)
                 (when (eql option-type :go)
                   (return-from handshake-fixed-newstyle)))))
            (otherwise (reply-error option stream :unsup))))))))

(defun reply-info-export (option stream server)
  (reply option stream :info `(:export
                               ,(server-export-size server)
                               ,(logior nbd/lib/transmission-flags:*has-flags*
                                        (server-supported-flags server)))))

(defun reply-info-block-size (option stream server)
  (reply option stream :info `(:block-size
                               ,(server-min-block-size server)
                               ,(server-preferred-block-size server)
                               ,(server-max-block-size server))))

(defun handshake-newstyle (server client stream)
  (let ((option (read-option stream)))
    (unless (eql (option-type option) :export-name)
      (error
       "Invalid option, non-fixed newstyle must only send NBD_OPT_EXPORT_NAME"))
    (validate-export-name server (option-value option)))

  (handshake-export-name server client stream)

  (finish-output stream))

(defun validate-export-name (server name)
  (unless (string= name (server-export-name server))
    (error
     "Invalid export name")))

(defun handshake-export-name (server client stream)
  (write-integer (server-export-size server)
                 8
                 stream
                 :byte-order :big-endian)

  (write-integer
   (logior nbd/lib/transmission-flags:*has-flags*
           (server-supported-flags server))
   2
   stream
   :byte-order :big-endian)

  (unless (client-no-zeroes client)
    (write-integer 0 124 stream)))
