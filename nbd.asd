(defsystem "nbd"
  :author "Florian Margaine <florian@margaine.com>"
  :description "Network Block Device server library."
  :license "MIT"
  :defsystem-depends-on ("wild-package-inferred-system")
  :class "winfer:wild-package-inferred-system"
  :depends-on ("nbd/lib/*" "nbd/api"))

(defsystem "nbd/simple-in-memory"
  :defsystem-depends-on ("wild-package-inferred-system")
  :class "winfer:wild-package-inferred-system"
  :depends-on ("nbd" "nbd/simple-in-memory/*"))
